# -*- coding: utf-8 -*-
"""
This script is designed to pull sb removal by zone for a given site
Created on Thu Sep 17 14:10:32 2020

@author: William.Machamer
"""


#AFSDK imports
import os
import sys  
sys.path.append('C:\\Program Files (x86)\\PIPC\\AF\\PublicAssemblies\\4.0\\')     
import clr
clr.AddReference('OSIsoft.AFSDK')  
from OSIsoft.AF import *  
from OSIsoft.AF.PI import *  
from OSIsoft.AF.Asset import *  
from OSIsoft.AF.Analysis import *  
from OSIsoft.AF.Data import *  
from OSIsoft.AF.Time import *  
from OSIsoft.AF.Search import *
from OSIsoft.AF.EventFrame import * 
from matplotlib import pyplot as plt
import matplotlib
import datetime as dt
import numpy as np
import ctypes.wintypes
from win32com.shell import shellcon
import tkinter as tk
from tkinter import * 



#Call AF Servers  
afservers = PISystems()  
afserver = afservers.DefaultPISystem 
def kill_program(window):
    window.destroy()
    
def make_error_window(title,message):
    errorWindow = Tk()
    errorWindow.title(title)
    errorFrame = Frame(errorWindow)
    errorFrame.grid(column=0,row=0, sticky=(N,W,E,S) )
    errorFrame.columnconfigure(0, weight = 1)
    errorFrame.rowconfigure(0, weight = 1)
    errorFrame.pack(pady = 10, padx = 10)
    Label(errorFrame, text = message).grid(row = 0, column = 0)
    okButton = tk.Button(errorFrame, text = "Okay", command = lambda:kill_program(errorWindow))
    okButton.grid(row = 1,column = 0)    

def get_sb_elements(site):
    '''Returns an AFElements object of sootblower elements for site.
    site: Name of the site'''
    ##Grabs all top level elements from databases to figure out which db site is in
    this_db = [db for db in afserver.Databases if site in [ele.Name for ele in db.Elements]][0]
    this_query = "Name:'Sootblowers' Root:'" + site +"'"
    
    ##Finds the 'Sootblowers' Element
    this_sb_parent = list(AFElementSearch(this_db,'',this_query).FindElements())[0]
    
    ##Creates a list of sootblower elements
    this_sb_list = (this_sb_parent.Elements)    
    return(this_sb_list)

def get_removal_dicts(sb_srch,start_time,end_time):
    '''sb_srch: AFSearch Item of sootblowers\n
    start_time,end_time: string in format: mm/dd/yyyy with optional hh:mm:ss.fff\n
    '''
    ##Initiate empty dicts
    zone_dict = {};sb_dict = {}
    
    
    time_rng = AFTimeRange(start_time,end_time)
    
    ## Get the zone format and convert sb_srch to an indexable list
    sb_srch_list = list(sb_srch)
    this_sb = sb_srch_list[0]
    this_zones = list(this_sb.Parent.Parent.Elements.get_Item("Zones").Elements)
    this_atts = [y for x in [list(x.Attributes) for x in this_zones] for y in x if y.Name == "Zone_Type"]
    this_wt_zone_list = [this_zones[i].Name for i,x in enumerate(this_atts) if x.GetValue().Value == 1]
    this_zone_style = this_wt_zone_list[0]
    print("Found " + str(len(sb_srch_list)) + " Sootblowers at " + this_sb.GetPath().split("\\")[4])
    print("Gathering Sootblower Removal Data...")
    
    ## Gets removal values for each blower
    for ele in sb_srch_list:
        sb_dict[ele.Name] = {}
        atts = list(ele.Attributes)
        remins = [x for x in atts if x.Name == "RemovalInsert"][0]
        remret = [x for x in atts if x.Name == "RemovalRetract"][0]
        remzone = [x for x in atts if x.Name == "RemovalZone"][0]
        ins_vals = remins.GetValues(time_rng,100000,None).GetValueArrays([],[],[])
        ret_vals = remret.GetValues(time_rng,100000,None).GetValueArrays([],[],[])
        zone_vals = remzone.GetValues(time_rng,100000,None).GetValueArrays([],[],[])
        ins_len = len(ins_vals[1]);ret_len = len(ret_vals[1]);zone_len = len(zone_vals[1])
        len_check = ins_len != ret_len or ins_len != zone_len or ret_len != zone_len
        if len_check:
            len_min = min([ins_len,ret_len,zone_len])
            if len_min == ins_len:
                td_list = ins_vals[2]
            elif len_min == ret_len:
                td_list = ret_vals[2]
            else:
                td_list = zone_vals[2]
            ins_vals_fixed = [ins_vals[1][i] for i,x in enumerate(ins_vals[2]) if x in td_list]    
            ret_vals_fixed = [ret_vals[1][i] for i,x in enumerate(ret_vals[2]) if x in td_list]    
            zone_vals_fixed = [zone_vals[1][i] for i,x in enumerate(zone_vals[2]) if x in td_list]
            ins_len = len(ins_vals_fixed);ret_len = len(ret_vals_fixed);zone_len = len(zone_vals_fixed)
            len_min = min([ins_len,ret_len,zone_len])
            ins_vals_fixed = ins_vals_fixed[:len_min]
            ret_vals_fixed = ret_vals_fixed[:len_min]
            zone_vals_fixed = zone_vals_fixed[:len_min]
        else:
            td_list = ins_vals[2]
            ins_vals_fixed = [ins_vals[1][i] for i,x in enumerate(ins_vals[2]) if x in td_list]    
            ret_vals_fixed = [ret_vals[1][i] for i,x in enumerate(ret_vals[2]) if x in td_list]    
            zone_vals_fixed = [zone_vals[1][i] for i,x in enumerate(zone_vals[2]) if x in td_list]            


        ## Check to make sure everything is the right data type
        ins_bad_inds = [i for i,x in enumerate(ins_vals_fixed) if type(x) not in [float,int]]
        ret_bad_inds = [i for i,x in enumerate(ret_vals_fixed) if type(x) not in [float,int]]
        zone_bad_inds = [i for i,x in enumerate(zone_vals_fixed) if type(x) not in [float,int]]
        
        ##Remove bad data types
        all_inds = [y for x in [ins_bad_inds,ret_bad_inds,zone_bad_inds] for y in x]
        all_inds = list(set(all_inds))
        for ind in all_inds:
            ins_vals_fixed.pop(ind)
            ret_vals_fixed.pop(ind)
            zone_vals_fixed.pop(ind)
        
        ##Give zone names proper formatting
        zone_list = []    
        try:
            if "WT" in str.upper(this_zone_style):
                zones = ["WT{:02d}".format(x) for x in zone_vals_fixed]
            elif "ZONE" in str.upper(this_zone_style):
                zones = ["Zone{:02d}".format(x) for x in zone_vals_fixed]
            
            ##Remove zones not in actual WT zones
            zones = [x for x in zones if x in this_wt_zone_list]
            
            ##Capture unique zone names
            zone_list = list(set(zones))
    
        except:
            ''
        ## Populate zone and sb dicts with removals
        for zone in zone_list:
            try:
                zone_dict[zone]
            except:
                zone_dict[zone] = {}
            zone_dict[zone][ele.Name] = {"Insert":[],"Retract":[]}
            sb_dict[ele.Name][zone] = {"Insert":[],"Retract":[]}
            this_remins = [ins_vals_fixed[i] for i,x in enumerate(zones) if x == zone]
            this_remret = [ret_vals_fixed[i] for i,x in enumerate(zones) if x == zone]        
            zone_dict[zone][ele.Name]['Insert'] = this_remins
            zone_dict[zone][ele.Name]['Retract'] = this_remret
            sb_dict[ele.Name][zone]['Insert'] = this_remins
            sb_dict[ele.Name][zone]['Retract'] = this_remret
    return(sb_dict,zone_dict)

def plot_by_zone(zone_dict,site,start_time,end_time,save_loc):
    matplotlib.use('Agg')
    for zone in zone_dict:
        fig = plt.figure(zone + " - " + start_time + " to " + end_time)
        ins_list = [];ret_list = [];sb_list = []
        for sb in zone_dict[zone]:
            sb_list.append(sb)
            ins_list.append(abs(np.average(zone_dict[zone][sb]['Insert'])))
            ret_list.append(abs(np.average(zone_dict[zone][sb]['Retract'])))
        labels = ['Insert Removal','Retract Removal']
        plt.bar(sb_list,ins_list);plt.bar(sb_list,ret_list,bottom=ins_list)
        plt.xticks(rotation=45)

        try:
            plt.legend(labels)
        except:
            ''
        buf= ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
        ctypes.windll.shell32.SHGetFolderPathW(None, save_loc, None, 0, buf)
        directory = os.path.join(buf.value,site,"Removals"+dt.datetime.strptime(start_time,"%m/%d/%Y").strftime("%m%d%y") + "-"+dt.datetime.strptime(end_time,"%m/%d/%Y").strftime("%m%d%y"))
        if not os.path.exists(directory):
            os.mkdir(directory)
        fig_name = directory +"\\" + zone + ".png"
        fig.savefig(fig_name)

def plot_by_sb(sb_dict,site,start_time,end_time,save_loc):
    matplotlib.use('Agg')
    for sb in sb_dict:
        if len(sb_dict[sb]) > 0:
            fig = plt.figure(sb + " - " + start_time + " to " + end_time)
            ins_list = [];ret_list = [];zone_list = []
            for zone in sb_dict[sb]:
                zone_list.append(zone)
                ins_list.append(abs(np.average(sb_dict[sb][zone]['Insert'])))
                ret_list.append(abs(np.average(sb_dict[sb][zone]['Retract'])))
            labels = ['Insert Removal','Retract Removal']
            width = 0.33
            plt.bar(zone_list,ins_list,width = width);plt.bar(zone_list,ret_list,bottom=ins_list,width = width)
            plt.xticks(rotation=45)
            plt.legend(labels)
            buf= ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
            ctypes.windll.shell32.SHGetFolderPathW(None, save_loc, None, 0, buf)
            directory = os.path.join(buf.value,site,"Removals"+dt.datetime.strptime(start_time,"%m/%d/%Y").strftime("%m%d%y") + "-"+dt.datetime.strptime(end_time,"%m/%d/%Y").strftime("%m%d%y"))
            if not os.path.exists(directory):
                os.mkdir(directory)
            fig_name = directory + "\\" + sb + ".png"
            fig.savefig(fig_name)

def run_everything(site,start_time,end_time,window,sb_check,zone_check,loc):

    SHGFP_TYPE_CURRENT = 0   # Get current, not default value
    buf= ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
    ctypes.windll.shell32.SHGetFolderPathW(None, loc, None, SHGFP_TYPE_CURRENT, buf)
    directory = os.path.join(buf.value,site)
    if not os.path.exists(directory):
        os.mkdir(directory)
    sbs = get_sb_elements(site)
    sb_dict, zone_dict = get_removal_dicts(sbs,start_time,end_time)
    sb_dict_check = any([len(sb_dict[x]) for x in sb_dict])
    zone_dict_check = any([len(zone_dict[x]) for x in zone_dict])
    plt.rcParams.update({'figure.max_open_warning': 0})
    plt.close('all')
    sb_err = False; zone_err = False

    if zone_check:
        if not zone_dict_check:
            make_error_window("Zone Plots Error", "Unable to generate removal data")

        else:
            try:
                plot_by_zone(zone_dict,site,start_time,end_time,loc)
            except:
                zone_err = True
                make_error_window("Zone Plots Error","Error generating zone plots.\nError Message:\n" + str(e))
    if sb_check:
        if not sb_dict_check:
            make_error_window("SB Plots Error","Unable to generate removal data")

        else:
            try:
                plot_by_sb(sb_dict,site,start_time,end_time,loc)
            except:
                sb_err = True
                make_error_window("SB Plots Error","Error generating SB plots.\nError Message:\n" + str(e))

    if not sb_err and not zone_err:
        new_directory = os.path.join(buf.value,site,"Removals"+dt.datetime.strptime(start_time,"%m/%d/%Y").strftime("%m%d%y") + "-"+dt.datetime.strptime(end_time,"%m/%d/%Y").strftime("%m%d%y"))
        global completeWindow
        completeWindow = Tk()
        completeWindow.title("Successfully Generated")
        completeFrame = Frame(completeWindow)
        completeFrame.grid(column=0,row=0, sticky=(N,W,E,S) )
        completeFrame.columnconfigure(0, weight = 1)
        completeFrame.rowconfigure(0, weight = 1)
        completeFrame.pack(pady = 10, padx = 10)
        Label(completeFrame, text = "Graphs Successfully Generated\nFile Location: " + new_directory).grid(row = 0, column = 0)
        okButton = tk.Button(completeFrame, text = "Okay", command = lambda:kill_program(completeWindow))
        okButton.grid(row = 1,column = 0)
    plt.close('all')
    kill_program(window)


def make_window():
    root = Tk()
    root.title("Removal Plots Generator")
    
    # Add a grid
    mainframe = Frame(root)
    mainframe.grid(column=0,row=0, sticky=(N,W,E,S) )
    mainframe.columnconfigure(0, weight = 1)
    mainframe.rowconfigure(0, weight = 1)
    mainframe.pack(pady = 10, padx = 10)
    
    # Create a Tkinter variable
    tkvar = StringVar(root)
    
    # Set with options
    site_label = Label(mainframe,text = "Root Element (Site)")
    site_label.grid(row = 0, column = 0)
    site_box = Entry(mainframe)
    site_box.grid(row = 1, column = 0)    
    startLabel = Label(mainframe,text = "Start (mm/dd/yyyy)")
    startLabel.grid(row = 0, column = 2)
    start_date = Entry(mainframe)
    start_date.grid(row = 1, column = 2)
    end_label = Label(mainframe,text = "End (mm/dd/yyyy)")
    end_label.grid(row = 0, column = 3)
    end_date = Entry(mainframe)
    end_date.grid(row = 1, column = 3)
    type_label = Label(mainframe,text = "Which graphs?")
    type_label.grid(row = 2, column = 0)
    sb_check = IntVar(); zone_check = IntVar()
    Checkbutton(mainframe, text="SB vs. Zone", variable=sb_check).grid(row=3, column = 0)
    Checkbutton(mainframe, text="Zone vs. SB", variable=zone_check).grid(row=3, column = 1)
    place_label = Label(mainframe,text = "Which graphs?")
    place_label.grid(row = 2, column = 3)
    Checkbutton(mainframe, text="SB vs. Zone", variable=sb_check).grid(row=3, column = 0)
    Checkbutton(mainframe, text="Zone vs. SB", variable=zone_check).grid(row=3, column = 1)
    choices = {'Desktop':12,
               'My Docs':5}
    tkvar.set(list(choices)[1]) # set the default option
    
    loc_menu = OptionMenu(mainframe, tkvar, *choices)
    Label(mainframe, text="Save Location?").grid(row = 2, column = 3)
    loc_menu.grid(row = 3, column = 3)
    blnk_row = Label(mainframe,text = '')
    blnk_row.grid(row = 4,column= 2)
    exc_button = tk.Button(mainframe, text="GENERATE GRAPHS",command=lambda:run_everything(site_box.get(), start_date.get(), end_date.get(),root,sb_check.get(),zone_check.get(),choices[tkvar.get()]))
    exc_button.grid(row = 5, column = 3)

    root.mainloop()
    

make_window()